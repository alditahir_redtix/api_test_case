<?php

namespace App\Http\Controllers;

use App\Models\Merchant;
use Illuminate\Http\Request;


class MerchantController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index()
    {

        $products = Merchant::all();

        return response()->json($products);

    }

    public function create(Request $request)
    {
        $price = new Merchant();

        $price->merchant_name = $request->merchant_name;
        $price->email = $request->email;
        $price->description = $request->description;

        $price->save();

        return response()->json($price);
    }

    public function show($id)
    {
        $price = Merchant::find($id);

        return response()->json($price);
    }

    public function update(Request $request, $id)
    {
        $product = Merchant::find($id);

        $product->merchant_name = $request->input('merchant_name');
        $product->email = $request->input('email');
        $product->description = $request->input('description');

        $product->save();
        return response()->json($product);
    }

    public function destroy($id)
    {
        $product = Merchant::find($id);
        $product->delete();

        return response()->json('merchant removed successfully');
    }


}
