<?php

namespace App\Http\Controllers;

use App\Models\Price;
use Illuminate\Http\Request;


class PriceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index()
    {

        $products = Price::all();

        return response()->json($products);

    }

    public function create(Request $request)
    {
        $price = new Price;

        $price->product_id = $request->product_id;
        $price->merchant_id = $request->merchant_id;
        $price->qty = $request->qty;
        $price->price_amount = $request->price_amount;
        $price->is_active = $request->is_active;

        $price->save();

        return response()->json($price);
    }

    public function show($id)
    {
        $price = Price::find($id);

        return response()->json($price);
    }

    public function update(Request $request, $id)
    {
        $product = Price::find($id);

        $product->product_id = $request->input('product_id');
        $product->merchant_id = $request->input('merchant_id');
        $product->qty = $request->input('qty');
        $product->is_active = $request->input('is_active');


        $product->save();
        return response()->json($product);
    }

    public function destroy($id)
    {
        $product = Price::find($id);
        $product->delete();

        return response()->json('price removed successfully');
    }

    public function changeStatus($id){
        $product = Price::find($id);

        if($product->is_active == 0){
            $product->is_active = 1;
        }else{
            $product->is_active = 0;
        }

        $product->save();
        return response()->json($product);

    }


}
