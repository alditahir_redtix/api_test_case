<?php

namespace App\Http\Controllers;

use App\Models\ProductCategory;
use Illuminate\Http\Request;


class ProductCategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index()
    {

        $products = ProductCategory::all();

        return response()->json($products);

    }

    public function create(Request $request)
    {
        $price = new ProductCategory();

        $price->category_name = $request->category_name;
        $price->description = $request->description;

        $price->save();

        return response()->json($price);
    }

    public function show($id)
    {
        $price = ProductCategory::find($id);

        return response()->json($price);
    }

    public function update(Request $request, $id)
    {
        $product = ProductCategory::find($id);

        $product->category_name = $request->input('category_name');
        $product->description = $request->input('description');

        $product->save();
        return response()->json($product);
    }

    public function destroy($id)
    {
        $product = ProductCategory::find($id);
        $product->delete();

        return response()->json('Category removed successfully');
    }


}
