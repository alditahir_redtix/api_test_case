<?php
namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;


class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index()
    {

        $products = Product::all();

        return response()->json($products);

    }

    public function create(Request $request)
    {
        $product = new Product;

        $product->product_name= $request->product_name;
        $product->description= $request->description;
        $product->is_active= $request->is_active;
        $product->id_product_category= $request->id_product_category;

        $product->save();

        return response()->json($product);
    }

    public function show($id)
    {
        $product = Product::find($id);

        return response()->json($product);
    }

    public function update(Request $request, $id)
    {
        $product= Product::find($id);

        $product->product_name = $request->input('product_name');
        $product->description = $request->input('description');
        $product->is_active = $request->input('is_active');
        $product->id_product_category = $request->input('id_product_category');


        $product->save();
        return response()->json($product);
    }

    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();

        return response()->json('product removed successfully');
    }


}
