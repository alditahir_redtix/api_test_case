<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix'=>'merchant'], function() use($router){
    $router->get('all', 'MerchantController@index');
    $router->get('show/{id}', 'MerchantController@show');
    $router->post('create', 'MerchantController@create');
    $router->post('update/{id}', 'MerchantController@update');
    $router->delete('destroy/{id}', 'MerchantController@destroy');
});

$router->group(['prefix'=>'price'], function() use($router){
    $router->get('all', 'PriceController@index');
    $router->get('show/{id}', 'PriceController@show');
    $router->post('create', 'PriceController@create');
    $router->post('update/{id}', 'PriceController@update');
    $router->post('changeStatus/{id}', 'PriceController@changeStatus');
    $router->delete('destroy/{id}', 'PriceController@destroy');
});

$router->group(['prefix'=>'product'], function() use($router){
    $router->get('all', 'ProductController@index');
    $router->get('show/{id}', 'ProductController@show');
    $router->post('create', 'ProductController@create');
    $router->post('update/{id}', 'ProductController@update');
    $router->delete('destroy/{id}', 'ProductController@destroy');
});

$router->group(['prefix'=>'productCategory'], function() use($router){
    $router->get('all', 'ProductCategoryController@index');
    $router->get('show/{id}', 'ProductCategoryController@show');
    $router->post('create', 'ProductCategoryController@create');
    $router->post('update/{id}', 'ProductCategoryController@update');
    $router->delete('destroy/{id}', 'ProductCategoryController@destroy');
});
